# Petstore on F5 Distributed Cloud

## Prerequisites

- [ ] F5XC Node up, running & registered
- [ ] Site with specific tag, for example: ves.io/siteName:\[myName\]
- [ ] Virtual site with site selector - Shared Configuration > Manage > Virtual Sites, Site Type: CE, Site Selector Epression: ves.io/siteName == \[myName\]


## Prepare environment

- [ ] Create new namespace - Administration > Personal Management > My Namespaces
- [ ] Create vK8s in new namespace - Distributed Apps > Applications > Virtual K8s, create \[myvK8s\], select you virtual site

## App deployment from gui

- [ ] Open \[myvK8s], deployment, create [deployment](https://gitlab.com/f5xc-labs/petstore-f5xc/-/blob/main/petstore-deployment.yaml)
    - [ ] Modify SWAGGER_HOST and SWAGGER_URL to fit your future public URL - \[yourname].\[domain]
- [ ] Create [service](https://gitlab.com/f5xc-labs/petstore-f5xc/-/blob/main/petstore-service.yaml)

## App deployment by kubectl
- [ ] Download kubeconfig file for vk8s:
 ![vk8s kubeconfig](/images/vk8s-kubeconfig.png)
- [ ] Download [deployment](https://gitlab.com/f5xc-labs/petstore-f5xc/-/blob/main/petstore-deployment.yaml) and [service](https://gitlab.com/f5xc-labs/petstore-f5xc/-/blob/main/petstore-service.yaml)
- [ ] Modify SWAGGER_HOST and SWAGGER_URL in petstore-service.yaml to fit your future public URL - \[yourname].\[domain]
- [ ] Apply:

    `kubectl apply -f petstore-deployment.yaml --kubeconfig [myKubeconfig]`\
    `kubectl apply -f petstore-service.yaml --kubeconfig [myKubeconfig]`

## Publish petshop
### Create origin pool
- [ ] Distributed Apps > Manage > Load Balancers > Origin Pools: Add Origin Pool
- [ ] Name: \[yourname-pool]
- [ ] Origin Servers: Add Item
  - [ ] Select Type of Origin Server: K8s Service Name of Origin Server on given Sites
  - [ ] Service Name: \[service].\[namespace], for example service.jd-petstore
  - [ ] Site or Virtual Site: Virtual Site
  - [ ] Virtual Site: shared/\[yourVirtualSite]
  - [ ] Select Network on the site: vK8s Networks on Site
  - [ ] Add Item
- [ ] Port: 80
- [ ] List of Health Checks: Add Item: Create new Helth Check
 - [ ] Name: \[petstore-check]
 - [ ] HTTP HealthCheck: Configure
  - [ ] Apply
 - [ ] Continue
 - [ ] Save and Exit
 ![origin pool](/images/originPool.png)

### Create public load balancer
- [ ] Distributed Apps > Manage > Load Balancers > HTTP Load Balancers: Add HTTP load balancer
- [ ] Name: \[whatever]
- [ ] Domains: \[yourname].\[domain]
- [ ] Select Type of Load Balancer: HTTPS with Automatic Certificate
- [ ] Origin Pools: Add Item, Select your pool
- [ ] VIP Configuration: Advertise on Internet
- [ ] Save and Exit
- [ ] Test
 ![HTTP LB 1](/images/HTTP-LB-1.png)
 ![HTTP LB 2](/images/HTTP-LB-2.png)
 ![HTTP LB 3](/images/HTTP-LB-3.png)
